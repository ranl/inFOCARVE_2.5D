#ifndef CAMERA_H
#define CAMERA_H

#include "GLM.h"



class Camera
{
public:
	glm::vec3 position;
	glm::vec3 focus;

	glm::vec3 translation;//data from realsense tracking 
	glm::vec4 rotation;//data from realsense tracking 

	float modelMatrix[16];//data from metaio tracking


	float distFromFocus;
	float FoV;
	int xPixels, yPixels;

	glm::mat4 viewMat;
	glm::mat4 projMat;

	void Init(int screenWidth, int screenHeight);
	void Update();
	void Update(glm::mat4 modelMatrix);

	void Zoom(float zoomAmount);
	void Translate(glm::vec3 translateAmount);
	void Rotate(float rotateAmount);
	void RotateAll(glm::vec4 rotateParams);
	glm::vec3 GetViewDirection();
};


#endif