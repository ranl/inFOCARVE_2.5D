#ifndef GPU_RAYCASTER_H
#define GPU_RAYCASTER_H

#include <GL/glew.h>
#include <GL/freeglut.h>
#include "Camera.h"
#include "GLM.h"
#include <vector>
#include "VolumeDataset.h"
#include "TransferFunction.h"
#include <string>

#include<iostream>

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "XToon.h"

class GPURaycaster
{
public:
	int maxRaySteps;
	float rayStepSize, gradientStepSize;
	GLuint rColorTex2D, rDepthTex2D, rDepthTex3D;
	glm::vec3 lightPosition;

	XToon xToon;


	GPURaycaster(int screenWidth, int screenHeight, VolumeDataset &volume);
	void Raycast(GLuint currTexture3D, TransferFunction &transferFunction, GLuint shaderProgramID, Camera &camera, IplImage * inDepth, IplImage * inColor);
	void Raycast(GLuint currTexture3D, TransferFunction &transferFunction, GLuint shaderProgramID, Camera &camera);
};

#endif

