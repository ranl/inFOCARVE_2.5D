/*
 * BackgroundRenderer.h
 *
 *  Created on: 24 Feb 2015
 *      Author: GV2LabPC
 */

#ifndef BACKGROUNDRENDERER_H_
#define BACKGROUNDRENDERER_H_

#include <GL/glew.h>
#include <windows.h>

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "RealSense.h"

class BackgroundRenderer
{
public:

	void Init();
	void DrawBackground(int shaderID, GLuint backgroundTex, GLuint volumeTex);
	void DrawBackground(int shaderID, GLuint backgroundTex, GLuint volumeTex, const GLvoid * backgroundDepthTex);

	GLuint mVboTexCoords;
	GLuint mVboPosition;
	GLuint mVao;
	GLuint inDepthTex2D;
	bool mBlackAndWhite;



private:

};


#endif /* BACKGROUNDRENDERER_H_ */
