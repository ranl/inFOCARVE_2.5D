#ifndef REAL_SENSE_H
#define REAL_SENSE_H

#include "pxcsession.h"
#include "pxccapture.h"
#include "pxchandmodule.h"
#include "pxchanddata.h"
#include "pxcsensemanager.h"
#include "pxccapture.h"
#include "pxcvideomodule.h"
#include "pxchandconfiguration.h"
#include "pxcprojection.h"
#include <iostream>
#include "GLM.h"
#include <vector>
#include "ShaderManager.h"
#include "Camera.h"
#include "Constants.h"

#include "opencv2\opencv.hpp"
#include "opencv2\imgproc\imgproc.hpp"

class RealSense
{
public:
	PXCSession *session;
	PXCSenseManager *pp;
	PXCHandConfiguration* config;
	PXCHandData* outputData;
	PXCProjection *projection;
	PXCImage::ImageData dataColor;

	std::vector<PXCHandData::JointData> joints;
	std::vector<PXCPoint3DF32> uvz;
	std::vector<PXCPointF32> depthCoord2color;

	std::vector<glm::vec3> handPoints;
	int numHandPoints;
	
	float maxRange;

	PXCCapture::Sample *sample;
	PXCScenePerception *perception;


	RealSense();
	~RealSense();

	bool Update();
	void ConnectJoints();
	void DrawPoints(Camera &camera, ShaderManager &shaderManager);

};


#endif