#ifndef OPENGL_RENDERER_H
#define OPENGL_RENDERER_H

#include "VolumeDataset.h"
#include "ShaderManager.h"
#include "GPURaycaster.h"
#include "TransferFunction.h"


#include "TFOptimizer.h"
#include "IntensityTFOptimizer.h"
#include "IntensityTFOptimizerV2.h"
#include "VisibilityTFOptimizer.h"
#include "VisibilityHistogram.h"


#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"

class OpenGLRenderer
{
public:
	GPURaycaster *raycaster;
	TransferFunction transferFunction;
	GLuint currTexture3D;
	int textureSize;

	TFOptimizer *_optimizer;
	IntensityTFOptimizer *_intensityTFOptimizer;
	IntensityTFOptimizerV2 *_intensityTFOptimizerV2;
	VisibilityHistogram visibilityHistogram;
	BOOL isOptimized;

	OpenGLRenderer(int screenWidth, int screenHeight, VolumeDataset &volume, ShaderManager &shaderManager, Camera &camera);
	void UpdateTexture(int currentTimestep, VolumeDataset &volume);
	GLuint GenerateTexture(VolumeDataset &volume);
	void Draw(VolumeDataset &volume, ShaderManager &shaderManager, Camera &camera, IplImage * inDepth, IplImage * inColor);
	void Draw(VolumeDataset &volume, ShaderManager &shaderManager, Camera &camera);
};

#endif