/*
 * BackgroundRenderer.cpp
 *
 *  Created on: 24 Feb 2015
 *      Author: GV2LabPC
 */


#include "BackgroundRenderer.h"




void BackgroundRenderer::Init()
{

	//LOGI("NativeCam_Init-------GenTextures");

	GLfloat mCubeTextureCoordinateData[] =
	                    {
	                            //region Coords
	                            // Front face
	                            0.0f, 0.0f,
	                            1.0f, 0.0f,
	                            0.0f, 1.0f,
	                            1.0f, 1.0f,
	                    };
	GLfloat mCubeData[] =
	            {
	            		-1.0f,-1.0f,0.0f,
	            		1.0f,-1.0f,0.0f,
	            		-1.0f,1.0f,0.0f,
	            		1.0f,1.0f,0.0f
	            };

		mVboTexCoords = 0;
		mVboPosition = 0;
	    glGenBuffers (1, &mVboPosition);
	    glBindBuffer (GL_ARRAY_BUFFER, mVboPosition);
	    glBufferData (GL_ARRAY_BUFFER,12 * sizeof(float),mCubeData,GL_STATIC_DRAW);
	    glBindBuffer (GL_ARRAY_BUFFER, 0);


	    glGenBuffers (1, &mVboTexCoords);
	    glBindBuffer (GL_ARRAY_BUFFER, mVboTexCoords);
	    glBufferData (GL_ARRAY_BUFFER,8 * sizeof(float),mCubeTextureCoordinateData,GL_STATIC_DRAW);
	    //checkGlError("NativeCam_INIT------TexCoords");
	    glBindBuffer (GL_ARRAY_BUFFER, 0);

	    mVao = 0;
	    glGenVertexArrays(1,&mVao);
	    glBindVertexArray(mVao);
	    glBindBuffer(GL_ARRAY_BUFFER,mVboPosition);
	    glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	    glBindBuffer(GL_ARRAY_BUFFER, mVboTexCoords);
	    glVertexAttribPointer (1, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	    glEnableVertexAttribArray(0);
	    glEnableVertexAttribArray(1);
	    //checkGlError("NativeCam_INIT------EnableVertAttribArray");

	    glBindBuffer (GL_ARRAY_BUFFER, 0);

}

void BackgroundRenderer::DrawBackground(int shaderID, GLuint backgroundTex, GLuint volumeTex, IplImage * inDepthImage)
{


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,backgroundTex);
	GLuint loc = glGetUniformLocation(shaderID, "backgroundTexture");
	glUniform1i(loc,0);

	////for depth image test
	//loc = glGetUniformLocation(shaderID, "depthImageShader");
	//glUniform1i(loc,2);
	glLoadIdentity();
	float x = 0, y = 0, z = 0;
			glBegin(GL_POINTS);
			for (int i=0;i<inDepthImage->height;i++)
			{
				for(int j=0;j<inDepthImage->width;j++){
					float posx=0,posy=0;

					float pixelDepth = cvGet2D(inDepthImage,i,j).val[0];

					x=((float)j-inDepthImage->height/2)/360;  
					y=((float)i-inDepthImage->width/2)/640;  
					z=-(pixelDepth)/255; 
					glColor3f(cvGet2D(inDepthImage,i,j).val[2]/255,cvGet2D(inDepthImage,i,j).val[1]/255,cvGet2D(inDepthImage,i,j).val[0]/255);
					//glColor3f(pixelDepth/255,0/255,0/255);
					glVertex3f(x,y,z);
				}
			}
			glEnd();
//	LOGI("LOC: %d",loc);
	//checkGlError("___BACKHGROUND TEX___ LOC: %d");

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D,volumeTex);
	loc = glGetUniformLocation(shaderID, "volumeTexture");
    glUniform1i(loc,1);

	

//	LOGI("LOC: %d",loc);
	//checkGlError("___VOLUME TEX___ LOC: ");

	glBindVertexArray(mVao);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	//checkGlError("glDrawArrays");


	glBindTexture(GL_TEXTURE_2D,0);
	glBindVertexArray(0);

}



