#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <GL/glew.h>


class Framebuffer
{
public:
	GLuint ID;
	int xPixels, yPixels;

	Framebuffer(int xPixels_, int yPixels_, GLuint tex);

	void ChangeTexture(GLuint tex);
	void Bind();

	void Unbind();
};

#endif
