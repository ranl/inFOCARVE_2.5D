#ifndef PHYSICS_WORLD
#define PHYSICS_WORLD

#include "GLM.h"
#include "CudaHeaders.h"
#include <vector>
#include "Camera.h"
#include "ShaderManager.h"
#include <time.h>
#include "ParticleSystem.h"
#include "Constants.h"
#include "Plane.h"
#include "Fluid.h"
#include <time.h>
#include "Cloth.h"

#define MAX_PER_CELL 12
#define MAX_PARTICLES 1000000

class PhysicsWorld
{
public:
	int stabilizationIters, solverIters;
	int cellsPerAxis, totalCellCount;

	ParticleSystem particleSystem;
	std::vector<glm::vec3> hostPositions;

	std::vector<Plane> planes;

	Fluid fluid;
	Cloth cloth;
	bool hoseOn;
	clock_t lastHoseTime;

	int *neighbours, *numNeighbours;
	int *particlesInCell, *numInCell;

	int numSceneParticles;
	glm::vec3 *sceneParticles;
	int *collisionNeighbours, *numCollisionNeighbours;

	void Init();
	void Update(int numSceneParticles_, glm::vec3 *sceneParticles_);
	void Draw(Camera &camera, ShaderManager &shaderManager);

	void InitCuda();
	void InitPlanes();

	void FindNeighbours();
	void ResolveCollisions();
};


#endif