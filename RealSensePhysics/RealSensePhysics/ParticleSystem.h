#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include "CudaHeaders.h"
#include "GLM.h"
#include "Constants.h"

class ParticleSystem
{
public:
	int numParticles;
	glm::vec3 *positions;
	glm::vec3 *newPositions;
	glm::vec3 *velocities;
	glm::vec3 *deltaPositions;
	float *masses;
	int *phaseIDs;

	float *lambda;
	float *densities;
	glm::vec3 *curvatureNormals;
	glm::vec3 *curls;

	void Init(int maxParticles);
};


#endif