#include "PhysicsWorld.h"

__global__ void CudaPredict(int numParticles, ParticleSystem particleSystem, float timestep, float gravityAccel)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

//	if (numParticles > 0)
//		printf("here");

	if (tid < numParticles)
	{
		glm::vec3 gravity = glm::vec3(0.0f, gravityAccel, 0.0f);

//		printf("%d: %f, %f, %f\n", tid, particleSystem.velocities[tid].x, particleSystem.velocities[tid].y, particleSystem.velocities[tid].z);

		particleSystem.velocities[tid] = particleSystem.velocities[tid] + (timestep * gravity);
		particleSystem.newPositions[tid] = particleSystem.positions[tid] + (timestep * particleSystem.velocities[tid]);

//		printf("%d: %f, %f, %f\n", tid, particleSystem.velocities[tid].x, particleSystem.velocities[tid].y, particleSystem.velocities[tid].z);
//		printf("%d: %f, %f, %f\n", tid, particleSystem.newPositions[tid].x, particleSystem.newPositions[tid].y, particleSystem.newPositions[tid].z);
	}
}

__global__ void CudaUpdateVelocities(int numParticles, ParticleSystem particleSystem, float timestep)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numParticles)
	{
		particleSystem.velocities[tid] = (particleSystem.newPositions[tid] - particleSystem.positions[tid]) / timestep;
	}
}

__global__ void CudaUpdatePositions(int numParticles, ParticleSystem particleSystem, float timestep)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numParticles)
	{
		particleSystem.positions[tid] = particleSystem.newPositions[tid];
	}
}

__global__ void CudaClearCells (int* numInCell,int totalCellCount)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < totalCellCount)
	{
		numInCell[tid] = 0;
	}
}

__global__ void CudaFillCells (int numParticles, ParticleSystem particleSystem, int* particlesInCell, int* numInCell, float particleDiameter, float extents, int cellsPerAxis)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numParticles)
	{
		int x = (int)((particleSystem.newPositions[tid].x + extents) / particleDiameter);
		int y = (int)((particleSystem.newPositions[tid].y + extents) / particleDiameter);
		int z = (int)((particleSystem.newPositions[tid].z + extents) / particleDiameter);

		if (x < 0 || x >= cellsPerAxis || y < 0 || y >= cellsPerAxis || z < 0 || z >= cellsPerAxis)
			return;

		int cellID = x + (y * cellsPerAxis) + (z * cellsPerAxis * cellsPerAxis);

		int oldCounter = atomicAdd(numInCell + cellID, 1);

		if (oldCounter < MAX_PER_CELL)
			particlesInCell[(cellID * MAX_PER_CELL) + oldCounter] = tid;
	}
}

__global__ void CudaFillCells2 (int numParticles, int numSceneParticles, glm::vec3 *sceneParticles, int* particlesInCell, int* numInCell, float particleDiameter, float extents, int cellsPerAxis)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);
	
	if (tid < numSceneParticles)
	{
	
		int x = (int)((sceneParticles[tid].x + extents) / particleDiameter);
		int y = (int)((sceneParticles[tid].y + extents) / particleDiameter);
		int z = (int)((sceneParticles[tid].z + extents) / particleDiameter);

		if (x < 0 || x >= cellsPerAxis || y < 0 || y >= cellsPerAxis || z < 0 || z >= cellsPerAxis)
			return;

		int cellID = x + (y * cellsPerAxis) + (z * cellsPerAxis * cellsPerAxis);

		int oldCounter = atomicAdd(numInCell + cellID, 1);

		if (oldCounter < MAX_PER_CELL)
			particlesInCell[(cellID * MAX_PER_CELL) + oldCounter] = numParticles + tid;
	}
	
}


__global__ void CudaAddNeighbours (int numParticles, ParticleSystem particleSystem, glm::vec3* sceneParticles, int* neighbours, int* numNeighbours, int* collisionNeighbours, int* numCollisionNeighbours, int* particlesInCell, int* numInCell, float particleDiameter, float extents, int cellsPerAxis, int totalCellCount)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numParticles)
	{
		glm::vec3 pos = particleSystem.newPositions[tid];

		int x = (int)((pos.x + extents) / particleDiameter);
		int y = (int)((pos.y + extents) / particleDiameter);
		int z = (int)((pos.z + extents) / particleDiameter);

		float particleDiameter2 = particleDiameter * particleDiameter;

		int calcID;
		int counter = 0;
		int collisionCounter = 0;
		int* tidAddress = neighbours + (tid * MAX_NEIGHBOURS);
		int* collisionTidAddress = collisionNeighbours + (tid * MAX_NEIGHBOURS);

		for (int i = z - 1; i <= z + 1; i++)
		{
			for (int j = y - 1; j <= y + 1; j++)
			{
				for (int k = x - 1; k <= x + 1; k++)
				{
					calcID = k + (j * cellsPerAxis) + (i * cellsPerAxis * cellsPerAxis);

					if (calcID < 0 || calcID >= totalCellCount)
						continue;

					for (int l=0; l<numInCell[calcID] && l<MAX_PER_CELL; l++)
					{
						int p2 = particlesInCell[(calcID * MAX_PER_CELL) + l];

						if (p2 >= numParticles)
						{
							if (collisionCounter < MAX_NEIGHBOURS)
							{
								if (glm::distance2(pos, sceneParticles[p2 - numParticles]) < particleDiameter2)
								{
									collisionTidAddress[collisionCounter] = p2;
									collisionCounter++;
								}
							}
						}
						else
						{
							if (tid == p2)
								continue;

							if (particleSystem.phaseIDs[tid] != particleSystem.phaseIDs[p2] || particleSystem.phaseIDs[tid] == 1)
							{
								if (collisionCounter < MAX_NEIGHBOURS)
								{
									if (glm::distance2(pos, particleSystem.newPositions[p2]) < particleDiameter2)
									{
										collisionTidAddress[collisionCounter] = p2;
										collisionCounter++;
									}
								}
							}
							else if (particleSystem.phaseIDs[tid] == 0)
							{
								if (counter < MAX_NEIGHBOURS)
								{
									if (glm::distance2(pos, particleSystem.newPositions[p2]) < particleDiameter2)
									{
										tidAddress[counter] = p2;
										counter++;
									}
								}
							}
						}
					}
				}
			}
		}
		numNeighbours[tid] = counter;
		numCollisionNeighbours[tid] = collisionCounter;
	}
}

__global__ void CudaResolveCollisions(int numParticles, ParticleSystem particleSystem, glm::vec3 *sceneParticles, int* collisionNeighbours, int* numCollisionNeighbours, float particleDiameter)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numParticles && numCollisionNeighbours[tid] > 0)
	{
//		printf("collision\n");
		float dist;
		particleSystem.deltaPositions[tid] = glm::vec3(0.0f);

		for (int i=0; i<numCollisionNeighbours[tid] && i<MAX_NEIGHBOURS; i++)
		{
			int calcID = collisionNeighbours[(tid * MAX_NEIGHBOURS) + i];

			if (calcID >= numParticles)
			{
				calcID -= numParticles;
				dist = glm::distance(particleSystem.newPositions[tid], sceneParticles[calcID]);
				particleSystem.deltaPositions[tid] -= (dist - particleDiameter) * (particleSystem.newPositions[tid] - sceneParticles[calcID]) / dist; // (1.0f / 2.0f) *  must include something for mass
			}
			else
			{
				dist = glm::distance(particleSystem.newPositions[tid], particleSystem.newPositions[calcID]);
				particleSystem.deltaPositions[tid] -= (1.0f / 2.0f) * (dist - particleDiameter) * (particleSystem.newPositions[tid] - particleSystem.newPositions[calcID]) / dist;
			}
		}

		particleSystem.deltaPositions[tid] = particleSystem.deltaPositions[tid] / (float)numCollisionNeighbours[tid];
	}
}

__global__ void CudaUpdateNewPos(int numParticles, ParticleSystem particleSystem)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numParticles)
	{
		particleSystem.newPositions[tid] += particleSystem.deltaPositions[tid];			// Check if memcpy an array of ints fro cpu is faster
	}
}



void PhysicsWorld::Init()
{
	stabilizationIters = 2;
	solverIters = 2;
	cellsPerAxis = (boundsExtent * 2.0f) / particleDiameter;
	totalCellCount = cellsPerAxis * cellsPerAxis * cellsPerAxis;
	
	particleSystem.Init(MAX_PARTICLES);
	hostPositions.resize(MAX_PARTICLES);

//	fluid = Fluid(13, particleSystem);
//	fluid = Fluid(true);
	cloth = Cloth(glm::vec3(0.0f, 1.25f, -1.0f), 25, particleDiameter, particleSystem, 0.4f);
	hoseOn = false;

	InitCuda();
	InitPlanes();
}

void PhysicsWorld::InitCuda()
{
	HANDLE_ERROR( cudaMalloc((void**)&neighbours, MAX_NEIGHBOURS * MAX_PARTICLES * sizeof(int)) );
	HANDLE_ERROR( cudaMalloc((void**)&numNeighbours, MAX_PARTICLES * sizeof(int)) );

	HANDLE_ERROR( cudaMalloc((void**)&particlesInCell, MAX_PER_CELL * totalCellCount * sizeof(int)) );
	HANDLE_ERROR( cudaMalloc((void**)&numInCell, totalCellCount * sizeof(int)) );

	HANDLE_ERROR( cudaMalloc((void**)&sceneParticles, MAX_PARTICLES * sizeof(glm::vec3)) );

	HANDLE_ERROR( cudaMalloc((void**)&collisionNeighbours, MAX_NEIGHBOURS * MAX_PARTICLES * sizeof(int)) );
	HANDLE_ERROR( cudaMalloc((void**)&numCollisionNeighbours, MAX_PARTICLES * sizeof(int)) );
}

void PhysicsWorld::InitPlanes()
{
	float cubeWidth = 1.0f;

	planes.push_back(Plane(glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
	planes.push_back(Plane(glm::vec3(-cubeWidth, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
	planes.push_back(Plane(glm::vec3(cubeWidth, 0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f)));
	planes.push_back(Plane(glm::vec3(0.0f, 0.0f, -cubeWidth), glm::vec3(0.0f, 0.0f, 1.0f)));
	planes.push_back(Plane(glm::vec3(0.0f, 0.0f, cubeWidth), glm::vec3(0.0f, 0.0f, -1.0f)));
}


void PhysicsWorld::Update(int numSceneParticles_, glm::vec3 *sceneParticles_)
{
	numSceneParticles = numSceneParticles_;
	HANDLE_ERROR( cudaMemcpy(sceneParticles, sceneParticles_, numSceneParticles * sizeof(glm::vec3), cudaMemcpyHostToDevice) );

	if (hoseOn)
	{
		clock_t currTime = clock();
		float timeElapsed =  (currTime - lastHoseTime) / (float) CLOCKS_PER_SEC;
		if (timeElapsed > 0.15f)
		{
			fluid.Hose(particleSystem);
			lastHoseTime = currTime;
		}
	}

	CudaPredict<<< (particleSystem.numParticles + (threadsPerBlock - 1)), threadsPerBlock >>> (particleSystem.numParticles, particleSystem, timestep, gravity);

	FindNeighbours();

	int iter = 0;
	
	while (iter < stabilizationIters)
	{
		for (int i=0; i<planes.size(); i++)
			planes[i].Constrain(particleSystem.numParticles, particleSystem);

		ResolveCollisions();

		cloth.Constrain(particleSystem);
		cloth.ClampPoints(particleSystem);

//		fluid.Constrain(particleSystem, neighbours, numNeighbours);	
		CudaUpdateNewPos <<<(particleSystem.numParticles + (threadsPerBlock - 1)), threadsPerBlock>>> (particleSystem.numParticles, particleSystem);

		iter++;
	}

	CudaUpdateVelocities<<<(particleSystem.numParticles + (threadsPerBlock - 1)), threadsPerBlock>>> (particleSystem.numParticles, particleSystem, timestep);
//	fluid.ModifyVelocity(particleSystem, neighbours, numNeighbours, timestep);
	CudaUpdatePositions<<<(particleSystem.numParticles + (threadsPerBlock - 1)), threadsPerBlock>>> (particleSystem.numParticles, particleSystem, timestep);

	HANDLE_ERROR( cudaMemcpy(&hostPositions[0], particleSystem.positions, particleSystem.numParticles * sizeof(glm::vec3), cudaMemcpyDeviceToHost) );
}



void PhysicsWorld::FindNeighbours()
{
	CudaClearCells <<<(totalCellCount + 255) / 256, 256>>> (numInCell, totalCellCount);

	CudaFillCells <<<(particleSystem.numParticles + (threadsPerBlock - 1)), threadsPerBlock>>> (particleSystem.numParticles, particleSystem, particlesInCell, numInCell, particleDiameter, boundsExtent, cellsPerAxis);

	CudaFillCells2 <<<(numSceneParticles + 255) / 256, 256>>> (particleSystem.numParticles, numSceneParticles, sceneParticles, particlesInCell, numInCell, particleDiameter, boundsExtent, cellsPerAxis);


	CudaAddNeighbours <<<(particleSystem.numParticles + (threadsPerBlock - 1)), threadsPerBlock>>> (particleSystem.numParticles, particleSystem, sceneParticles, neighbours, numNeighbours, collisionNeighbours, numCollisionNeighbours, particlesInCell, numInCell, particleDiameter, boundsExtent, cellsPerAxis, totalCellCount);
}

void PhysicsWorld::ResolveCollisions()
{

	CudaResolveCollisions <<<(particleSystem.numParticles + (threadsPerBlock - 1)), threadsPerBlock>>> (particleSystem.numParticles, particleSystem, sceneParticles, collisionNeighbours, numCollisionNeighbours, particleDiameter);
	cudaDeviceSynchronize();
	CudaUpdateNewPos <<<(particleSystem.numParticles + (threadsPerBlock - 1)), threadsPerBlock>>> (particleSystem.numParticles, particleSystem);
	
}






void PhysicsWorld::Draw(Camera &camera, ShaderManager &shaderManager)
{
	GLuint shaderProgramID = shaderManager.UseShader(SphereShader);

	int uniformLoc;

	glm::mat4 model_mat = glm::mat4(1.0f);

	uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.projMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.viewMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &model_mat[0][0]);

	int texcoords_location = glGetAttribLocation (shaderProgramID, "vTexture");
	int radius_location = glGetAttribLocation (shaderProgramID, "pRadius");	

//	glm::mat4 upMat = glm::rotate(glm::mat4(), -camera.pitch, glm::vec3(1, 0, 0));
//	glm::vec3 upVec = glm::rotate(glm::quat_cast(upMat), glm::vec3(0, 1, 0));

	glm::vec3 camDirection = camera.GetViewDirection();

	glm::vec3 rightVec = glm::normalize(glm::cross(camDirection, glm::vec3(0.0f, 1.0f, 0.0f)));
	glm::vec3 upVec = glm::normalize(glm::cross(camDirection, -rightVec));
	
	glBegin(GL_QUADS);
	for (int i=0; i<particleSystem.numParticles; i++)
	{
		glm::vec3 posToCam = glm::normalize(hostPositions[i] - camera.position);
		glm::vec3 crossCam = glm::cross(posToCam, upVec);

		float radius = particleRadius * 1.0f;
		glVertexAttrib1f(radius_location, radius);

		glm::vec3 corner = hostPositions[i] - (radius * crossCam);
		corner += radius*upVec;

		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertexAttrib2f(texcoords_location, 0.0f, 0.0f);
		glVertex3f(corner.x, corner.y, corner.z);

		corner -= 2.0f*radius*upVec;

		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertexAttrib2f(texcoords_location, 0.0f, 1.0f);
		glVertex3f(corner.x, corner.y, corner.z);

		corner = hostPositions[i] + (radius * crossCam);
		corner -= radius*upVec;

		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertexAttrib2f(texcoords_location, 1.0f, 1.0f);
		glVertex3f(corner.x, corner.y, corner.z);

		corner += 2.0f*radius*upVec;

		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertexAttrib2f(texcoords_location, 1.0f, 0.0f);
		glVertex3f(corner.x, corner.y, corner.z);
	}
	glEnd();

}

