#ifndef ENVIRONMENT_H
#define	ENVIRONMENT_H

#include <string>
#include <GL/glew.h>
#include <il.h>
#include <ilu.h>
#include <ilut.h>
#include <iostream>
#include <vector>
#include <string>
#include "GLM.h"
#include "Camera.h"
#include "ShaderManager.h"

class EnvironmentMap
{
public:
	GLuint vaoID, vboID;
	GLuint m_textureObj;
	std::vector<GLenum> cube;
	std::vector<glm::vec3> cubePoints;
	GLuint frameBuffer;
	GLuint fbTex1, fbTex2;

	

	void Init();
	void InitBufferObjects();
	GLuint NewFrameBuffer();
	GLuint GenerateBufferTexture();
	void GetCubeTextures();
	void FlipTexture(Camera &camera, GLuint fromTex, GLuint toTex);

	void Load();
	void Update(Camera &camera);
	void Draw(Camera &camera, ShaderManager &shaderManager);

private:
    
    
	ILuint texid;
	ILboolean success;
};


#endif