#include "ParticleSystem.h"

__global__ void CudaMakeParticles(int numParticles, glm::vec3 *positions)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < (numParticles*numParticles*numParticles))
	{
		int remainder;
		int k = (int)(tid / (numParticles * numParticles));
		remainder = tid % (numParticles * numParticles);

		int j = (int)(remainder / numParticles);
		remainder = remainder % numParticles;

		int i = remainder;

		float gap = 0.6f;

		positions[tid] = glm::vec3(-3.0f + (gap * i), 3.0f + (gap * j), -3.0f + (gap * k));
	}
}

void ParticleSystem::Init(int maxParticles)
{
	numParticles = 0;

	HANDLE_ERROR( cudaMalloc((void**)&positions, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMalloc((void**)&newPositions, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMalloc((void**)&deltaPositions, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMalloc((void**)&velocities, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMalloc((void**)&masses, maxParticles * sizeof(float)) );
	HANDLE_ERROR( cudaMalloc((void**)&phaseIDs, maxParticles * sizeof(int)) );

	HANDLE_ERROR( cudaMalloc((void**)&lambda, maxParticles * sizeof(float)) );
	HANDLE_ERROR( cudaMalloc((void**)&densities, maxParticles * sizeof(float)) );
	HANDLE_ERROR( cudaMalloc((void**)&curvatureNormals, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMalloc((void**)&curls, maxParticles * sizeof(glm::vec3)) );



	HANDLE_ERROR( cudaMemset(positions, 0, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMemset(newPositions, 0, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMemset(deltaPositions, 0, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMemset(velocities, 0, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMemset(masses, 0, maxParticles * sizeof(float)) );
	HANDLE_ERROR( cudaMemset(phaseIDs, 0, maxParticles * sizeof(int)) );

	HANDLE_ERROR( cudaMemset(lambda, 0, maxParticles * sizeof(float)) );
	HANDLE_ERROR( cudaMemset(densities, 0, maxParticles * sizeof(float)) );
	HANDLE_ERROR( cudaMemset(curvatureNormals, 0, maxParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMemset(curls, 0, maxParticles * sizeof(glm::vec3)) );

//	int blah = 10;
//	numParticles = blah*blah*blah;
//	CudaMakeParticles<<<((blah*blah*blah) + (threadsPerBlock - 1)), threadsPerBlock>>> (blah, positions);
	
}