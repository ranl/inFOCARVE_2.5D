#version 330

in vec3 texcoords;
uniform samplerCube Cubemap;
out vec4 FragColor;

void main () 
{
	FragColor = texture (Cubemap, texcoords);
//	FragColor = vec4(1.0f, 0.0f, 0.0f,0.0f);
}