#include "Cloth.h"

Cloth::Cloth()
{ }

Cloth::Cloth(glm::vec3 pos, int particlesPerSide_, float particleDiameter_, ParticleSystem &particleSystem, float stretchStiffness_)
{
	particleDiameter = particleDiameter_ + (particleDiameter_ / 8.0f);
	particlesPerSide = particlesPerSide_;
	numClothParticles = particlesPerSide * particlesPerSide;
	stretchStiffness = stretchStiffness_;
	startingIndex = particleSystem.numParticles;

	int span = particlesPerSide / 2;
	float radius = particleDiameter / 2.0f;
	diagonalDistance = glm::sqrt(2.0f * glm::pow(particleDiameter, 2.0f));

	std::vector<glm::vec3> particlePositions;
	std::vector<glm::vec3> particleVelocities;
	std::vector<float> particleMasses;
	std::vector<int> particlePhaseIDs;

	for (int j = 0; j < particlesPerSide; j++)
	{
		for (int i = 0; i < particlesPerSide; i++)
		{
			if (particlesPerSide % 2 == 0)
//				particlePositions.push_back(pos + glm::vec3((i*particleDiameter) - (span * particleDiameter) + radius, 0.0f, (j*particleDiameter) - (span * particleDiameter) + radius));
				particlePositions.push_back(pos + glm::vec3((i*particleDiameter) - (span * particleDiameter) + radius, (j*particleDiameter) - (span * particleDiameter) + radius, 0.0f));
			else
				particlePositions.push_back(pos + glm::vec3((i*particleDiameter) - (span * particleDiameter), (j*particleDiameter) - (span * particleDiameter), 0.0f));

				
			particleVelocities.push_back(glm::vec3(0.0f));
			particleMasses.push_back((4.0f / 3.0f) * glm::pi<float>() * (glm::pow(radius, 3.0f)) * 1000.0f);
			particlePhaseIDs.push_back(1);

			clothParticleIndices.push_back(particleSystem.numParticles);
			particleSystem.numParticles++;
		}
	}

	const int matchedArraySizes = numClothParticles * 4 * sizeof(int);

	int* edgesArray = (int*)malloc(matchedArraySizes);
	int* diagonalsArray = (int*)malloc(matchedArraySizes);

	MatchParticles(edgesArray, diagonalsArray);
	
	HANDLE_ERROR( cudaMalloc((void**)&cudaClothParticles, numClothParticles * sizeof(int)) );
	HANDLE_ERROR( cudaMemcpy(cudaClothParticles, &clothParticleIndices[0], numClothParticles * sizeof(int), cudaMemcpyHostToDevice) );

	HANDLE_ERROR( cudaMemcpy(particleSystem.positions + startingIndex, &particlePositions[0], numClothParticles * sizeof(glm::vec3), cudaMemcpyHostToDevice) );			// add a starting index because always copies from start
	HANDLE_ERROR( cudaMemcpy(particleSystem.velocities + startingIndex, &particleVelocities[0], numClothParticles * sizeof(glm::vec3), cudaMemcpyHostToDevice) );
	HANDLE_ERROR( cudaMemcpy(particleSystem.masses + startingIndex, &particleMasses[0], numClothParticles * sizeof(float), cudaMemcpyHostToDevice) );
	HANDLE_ERROR( cudaMemcpy(particleSystem.phaseIDs + startingIndex, &particlePhaseIDs[0], numClothParticles * sizeof(int), cudaMemcpyHostToDevice) );
	
	HANDLE_ERROR( cudaMalloc((void**)&cudaMatchedEdges, matchedArraySizes) );
	HANDLE_ERROR( cudaMemcpy(cudaMatchedEdges, edgesArray, matchedArraySizes, cudaMemcpyHostToDevice) );

	HANDLE_ERROR( cudaMalloc((void**)&cudaMatchedDiagonals, matchedArraySizes) );
	HANDLE_ERROR( cudaMemcpy(cudaMatchedDiagonals, diagonalsArray, matchedArraySizes, cudaMemcpyHostToDevice) );

	
//	free(edgesArray);
//	free(diagonalsArray);

	CalculateTriangles();
	InitBufferObjects();
}


__global__ void CudaClampPoints(int numClothParticles, int *cudaClothParticles, ParticleSystem particleSystem, int particlesPerSide)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numClothParticles)
	{
		//if (tid == 0 || tid == (particlesPerSide-1))
		//	particleSystem.newPositions[cudaClothParticles[tid]] = particleSystem.positions[cudaClothParticles[tid]];


		if (tid == (numClothParticles-particlesPerSide) || tid == (numClothParticles -1))
			particleSystem.newPositions[cudaClothParticles[tid]] = particleSystem.positions[cudaClothParticles[tid]];
	}
}

void Cloth::ClampPoints(ParticleSystem &particleSystem)
{
	CudaClampPoints<<< (numClothParticles + 255) / 256, 256>>> (numClothParticles, cudaClothParticles, particleSystem, particlesPerSide);
}

void Cloth::MatchParticles(int *edgesArray, int *diagonalsArray)
{
	std::vector<std::vector<int>> matchedEdges;
	std::vector<std::vector<int>> matchedDiagonals;

	matchedEdges.resize(numClothParticles);
	matchedDiagonals.resize(numClothParticles);

	bool leftOpen, rightOpen, topOpen, botOpen;

	for (int i=0; i<numClothParticles; i++)
	{
		leftOpen = rightOpen = topOpen = botOpen = false;

		if ((i % particlesPerSide) != 0)
			leftOpen = true;			
		if ((i % particlesPerSide) != (particlesPerSide - 1))
			rightOpen = true;
		if ((i / (float)particlesPerSide) >= 1.0f)
			topOpen = true;			
		if ((i / (float)particlesPerSide) < (particlesPerSide - 1))
			botOpen = true;
			

		if(leftOpen)
			matchedEdges[i].push_back(clothParticleIndices[i - 1]);
		if(rightOpen)
			matchedEdges[i].push_back(clothParticleIndices[i + 1]);
		if(topOpen)
			matchedEdges[i].push_back(clothParticleIndices[i - particlesPerSide]);
		if(botOpen)
			matchedEdges[i].push_back(clothParticleIndices[i + particlesPerSide]);


		if (leftOpen && topOpen)
			matchedDiagonals[i].push_back(clothParticleIndices[i - (particlesPerSide + 1)]);
		if (leftOpen && botOpen)
			matchedDiagonals[i].push_back(clothParticleIndices[i + (particlesPerSide - 1)]);
		if (rightOpen && topOpen)
			matchedDiagonals[i].push_back(clothParticleIndices[i - (particlesPerSide - 1)]);
		if (rightOpen && botOpen)
			matchedDiagonals[i].push_back(clothParticleIndices[i + (particlesPerSide + 1)]);

		int count = 4-matchedEdges[i].size();

		for (int j=0; j<count; j++)
			matchedEdges[i].push_back(-1);
		
		count = 4-matchedDiagonals[i].size();

		for (int j=0; j<count; j++)
			matchedDiagonals[i].push_back(-1);
	}


	for (int i=0; i<numClothParticles; i++)
		for (int j=0; j<4; j++)
		{
			edgesArray[(i*4)+j] = matchedEdges[i][j];
			diagonalsArray[(i*4)+j] = matchedDiagonals[i][j];
		}
}



__global__ void CudaConstrain(int numParticles, int *cudaClothParticles, int *cudaMatchedEdges, int *cudaMatchedDiagonals, ParticleSystem particleSystem, float particleDiameter, float diagonalDistance)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numParticles)	
	{
		int currentParticleID = cudaClothParticles[tid];

		particleSystem.deltaPositions[currentParticleID] = glm::vec3(0.0f);
		
		for (int j=0; j<4; j++)
		{
			int matchedParticleID = cudaMatchedEdges[(tid*4) + j];

			if (matchedParticleID != -1)		// && matchedParticleID > currentParticleID
			{
				float dist = glm::distance(particleSystem.newPositions[currentParticleID], particleSystem.newPositions[matchedParticleID]);

				glm::vec3 delta = (1.0f / 2.0f) * (dist - particleDiameter) * (particleSystem.newPositions[currentParticleID] - particleSystem.newPositions[matchedParticleID]) / dist;

				particleSystem.deltaPositions[currentParticleID] -= delta;			
			}
		}
		
		for (int j=0; j<4; j++)
		{
			int matchedParticleID = cudaMatchedDiagonals[(tid*4) + j];

			if (matchedParticleID != -1)
			{
				float dist = glm::distance(particleSystem.newPositions[currentParticleID], particleSystem.newPositions[matchedParticleID]);

				glm::vec3 delta = (1.0f / 2.0f) * (dist - diagonalDistance) * (particleSystem.newPositions[currentParticleID] - particleSystem.newPositions[matchedParticleID]) / dist;

				particleSystem.deltaPositions[currentParticleID] -= delta;
			}
		}		
	}
}

__global__ void CudaUpdatePos(int numClothParticles, int *cudaClothParticles, ParticleSystem particleSystem, float stretchStiffness)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numClothParticles)
	{
		particleSystem.newPositions[cudaClothParticles[tid]] += stretchStiffness * particleSystem.deltaPositions[cudaClothParticles[tid]];
		particleSystem.deltaPositions[cudaClothParticles[tid]] = glm::vec3(0.0f);
	}
}


void Cloth::Constrain(ParticleSystem & particleSystem)
{
	CudaConstrain<<< (numClothParticles + 127) / 128, 128>>> (numClothParticles, cudaClothParticles, cudaMatchedEdges, cudaMatchedDiagonals, particleSystem, particleDiameter, diagonalDistance);
	cudaDeviceSynchronize();
	CudaUpdatePos<<< (numClothParticles + 127) / 128, 128>>> (numClothParticles, cudaClothParticles, particleSystem, stretchStiffness);
	cudaDeviceSynchronize();
	CalculateNormals(particleSystem);
}

void Cloth::CalculateTriangles()
{
	numTriangleVerts = (2 * 3) * (particlesPerSide - 1) * (particlesPerSide - 1);

	triangleIndices.reserve(numTriangleVerts);
	emptyNormals.reserve(numClothParticles);

	for (int j=0; j<(particlesPerSide - 1); j++)
	{
		for (int i=0; i<(particlesPerSide - 1); i++)
		{
			triangleIndices.push_back(i + (j * particlesPerSide));
			triangleIndices.push_back(i + particlesPerSide + (j * particlesPerSide));
			triangleIndices.push_back(i+1 + (j * particlesPerSide));

			triangleIndices.push_back(i+1 + (j * particlesPerSide));
			triangleIndices.push_back(i + particlesPerSide + (j * particlesPerSide));
			triangleIndices.push_back((i+1) + particlesPerSide + (j * particlesPerSide));					
		}
	}

	for (int i=0; i<numClothParticles; i++)
		emptyNormals.push_back(glm::vec3(0.0f));

	HANDLE_ERROR( cudaMalloc((void**)&cudaTriangleIndices, numTriangleVerts * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMemcpy(cudaTriangleIndices, &triangleIndices[0], numTriangleVerts * sizeof(glm::vec3), cudaMemcpyHostToDevice) );

	HANDLE_ERROR( cudaMalloc((void**)&cudaVertNormals, numClothParticles * sizeof(glm::vec3)) );
	HANDLE_ERROR( cudaMemcpy(cudaVertNormals, &emptyNormals[0], numClothParticles * sizeof(glm::vec3), cudaMemcpyHostToDevice) );
}



__global__ void CudaCalculateAerodynamics(int numTriangleverts, int *cudaClothParticles, ParticleSystem particleSystem, int *cudaTriangleIndices, glm::vec3 *cudaVertNormals)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid % 3 == 0 && tid < numTriangleverts)
	{
		int p1 = cudaClothParticles[cudaTriangleIndices[tid]];
		int p2 = cudaClothParticles[cudaTriangleIndices[tid + 1]];
		int p3 = cudaClothParticles[cudaTriangleIndices[tid + 2]];

		glm::vec3 faceVel = (particleSystem.velocities[p1] + particleSystem.velocities[p2] + particleSystem.velocities[p3]) / 3.0f;
		float speed = glm::length(faceVel);

		if (speed == 0.0f)
			return;

		glm::vec3 normVel = faceVel / speed;

		glm::vec3 cross = glm::cross((particleSystem.positions[p2] - particleSystem.positions[p1]), particleSystem.positions[p3] - particleSystem.positions[p1]);
		glm::vec3 faceNormal = glm::normalize(cross);
	}
}


void Cloth::Aerodynamics(ParticleSystem &particleSystem, float timestep)
{
	float dragCoefficient = 5.0f;
	float liftCoefficient = 1.0f;
	float airDensity = 10.0f;

	CudaCalculateAerodynamics <<<(numTriangleVerts + 255) / 256, 256>>> (numTriangleVerts, cudaClothParticles, particleSystem, cudaTriangleIndices, cudaVertNormals);

}


__global__ void CudaAccumulateNormals(int numTriangleverts, int *cudaClothParticles, ParticleSystem particleSystem, int *cudaTriangleIndices, glm::vec3 *cudaVertNormals)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid % 3 == 0 && tid < numTriangleverts)
	{
		
		int p1 = cudaClothParticles[cudaTriangleIndices[tid]];
		int p2 = cudaClothParticles[cudaTriangleIndices[tid + 1]];
		int p3 = cudaClothParticles[cudaTriangleIndices[tid + 2]];
		
		glm::vec3 v1 = particleSystem.positions[p2] - particleSystem.positions[p1];
		glm::vec3 v2 = particleSystem.positions[p3] - particleSystem.positions[p1];
		glm::vec3 v3 = glm::cross(v1, v2);
		v3 = glm::normalize(v3);
		
		atomicAdd(&cudaVertNormals[cudaTriangleIndices[tid]].x, v3.x);
		atomicAdd(&cudaVertNormals[cudaTriangleIndices[tid]].y, v3.y);
		atomicAdd(&cudaVertNormals[cudaTriangleIndices[tid]].z, v3.z);

		atomicAdd(&cudaVertNormals[cudaTriangleIndices[tid + 1]].x, v3.x);
		atomicAdd(&cudaVertNormals[cudaTriangleIndices[tid + 1]].y, v3.y);
		atomicAdd(&cudaVertNormals[cudaTriangleIndices[tid + 1]].z, v3.z);

		atomicAdd(&cudaVertNormals[cudaTriangleIndices[tid + 2]].x, v3.x);
		atomicAdd(&cudaVertNormals[cudaTriangleIndices[tid + 2]].y, v3.y);
		atomicAdd(&cudaVertNormals[cudaTriangleIndices[tid + 2]].z, v3.z);
		
	}
}

__global__ void CudaNormalizeNormals(int numClothParticles, glm::vec3 *cudaVertNormals)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numClothParticles)
	{
		float length = glm::length(cudaVertNormals[tid]);
		
		if (length > 0.0f)
			cudaVertNormals[tid] = cudaVertNormals[tid] / length;

//		printf("tid: %f, %f, %f\n", cudaVertNormals[tid].x, cudaVertNormals[tid].y, cudaVertNormals[tid].z);

	}
}



void Cloth::CalculateNormals(ParticleSystem &particleSystem)
{
	CudaAccumulateNormals <<<(numTriangleVerts + 255) / 256, 256>>> (numTriangleVerts, cudaClothParticles, particleSystem, cudaTriangleIndices, cudaVertNormals);
	CudaNormalizeNormals <<<(numClothParticles + 255) / 256, 256>>> (numClothParticles, cudaVertNormals);
}


void Cloth::Draw(GLuint shaderProgramID, ParticleSystem &particleSystem)
{
	std::vector<glm::vec3> positions;
	positions.resize(numClothParticles);
	HANDLE_ERROR(cudaMemcpy(&positions[0], particleSystem.positions + startingIndex, numClothParticles * sizeof(glm::vec3), cudaMemcpyDeviceToHost));

	std::vector<glm::vec3> normals;
	normals.resize(numClothParticles);
	HANDLE_ERROR(cudaMemcpy(&normals[0], cudaVertNormals, numClothParticles * sizeof(glm::vec3), cudaMemcpyDeviceToHost));

	std::vector<glm::vec3> bufferVector;
	bufferVector.reserve(4 * numTriangleVerts);

	for (int i=0; i<numTriangleVerts; i++)
	{
		bufferVector.push_back(positions[triangleIndices[i]] + (0.001f * normals[triangleIndices[i]]));
		bufferVector.push_back(normals[triangleIndices[i]]);
	}

	for (int i=0; i<numTriangleVerts; i++)
	{
		bufferVector.push_back(positions[triangleIndices[i]] + (-0.001f * normals[triangleIndices[i]]));
		bufferVector.push_back(-normals[triangleIndices[i]]);
	}

	glBindVertexArray(vaoID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);

	GLuint vPosition = glGetAttribLocation(shaderProgramID, "vPosition");
	glEnableVertexAttribArray(vPosition);
	
	GLuint vNormal = glGetAttribLocation(shaderProgramID, "vNormal");
	glEnableVertexAttribArray(vNormal);


	glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 2*sizeof(glm::vec3), 0);
	glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 2*sizeof(glm::vec3), BUFFER_OFFSET(sizeof(glm::vec3)));

	glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * numTriangleVerts * sizeof(glm::vec3), &bufferVector[0]);

	glDrawArrays(GL_TRIANGLES, 0, 2 * numTriangleVerts);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	HANDLE_ERROR( cudaMemcpy(cudaVertNormals, &emptyNormals[0], numClothParticles * sizeof(glm::vec3), cudaMemcpyHostToDevice) );
}


void Cloth::InitBufferObjects()
{
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);

	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);

	glBufferData(GL_ARRAY_BUFFER, 4 * numTriangleVerts * sizeof(glm::vec3), NULL, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}