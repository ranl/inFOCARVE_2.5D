#version 330

out vec4 FragColor;

in vec3 normal;
in vec3 lightDirection;
in float depth;

void main()
{
//	gl_FragDepth = depth;

	vec3 diffuse = vec3(0.3,0.3,0.3);
	vec3 ambient = vec3(0.4,0.4,0.4);

	vec3 n = normalize(normal);

	float intensity = max(dot(n, lightDirection), 0.0);

	vec3 color = ((intensity*diffuse) + ambient);

//	FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);

	FragColor = vec4(color, 1.0f);
}