#include "Fluid.h"

Fluid::Fluid() {}


Fluid::Fluid(int particlesPerSide, ParticleSystem &particleSystem)
{
	startingIndex = particleSystem.numParticles;

	glm::vec3 pos = glm::vec3(0.0f, 0.0f, 0.0f);
	int span = particlesPerSide / 2;

	numFluidParticles = particlesPerSide * particlesPerSide * (particlesPerSide * 10);

	std::vector<int> fluidParticleIndices;
	std::vector<glm::vec3> particlePositions;
	std::vector<glm::vec3> particleVelocities;
	std::vector<float> particleMasses;
	std::vector<int> particlePhaseIDs;

	for (int k = 0; k < particlesPerSide; k++)
	{
		for (int j = 0; j < (particlesPerSide * 10); j++)
		{
			for (int i = 0; i < particlesPerSide; i++)
			{
				particlePositions.push_back(pos + glm::vec3((i*particleDiameter*0.8f) - (span * particleDiameter*0.8f) + particleRadius, (j*particleDiameter*0.8f) - (span * particleDiameter*0.8f) + particleRadius, (k*particleDiameter*0.8f) - (span * particleDiameter*0.8f) + particleRadius));

				particleMasses.push_back((4.0f / 3.0f) * glm::pi<float>() * (glm::pow(particleRadius, 3.0f)) * REST_DENSITY);

				particleVelocities.push_back(glm::vec3(0.0f));
				particlePhaseIDs.push_back(0);

				fluidParticleIndices.push_back(particleSystem.numParticles);
				particleSystem.numParticles++;
			}
		}
	}

	HANDLE_ERROR( cudaMalloc((void**)&cudaFluidParticles, numFluidParticles * sizeof(int)) );		
	HANDLE_ERROR( cudaMemcpy(cudaFluidParticles, &fluidParticleIndices[0], numFluidParticles * sizeof(int), cudaMemcpyHostToDevice) );

	HANDLE_ERROR( cudaMemcpy(particleSystem.positions + startingIndex, &particlePositions[0], numFluidParticles * sizeof(glm::vec3), cudaMemcpyHostToDevice) );		// add a starting index because always copies from start
	HANDLE_ERROR( cudaMemcpy(particleSystem.velocities + startingIndex, &particleVelocities[0], numFluidParticles * sizeof(glm::vec3), cudaMemcpyHostToDevice) );
	HANDLE_ERROR( cudaMemcpy(particleSystem.masses + startingIndex, &particleMasses[0], numFluidParticles * sizeof(float), cudaMemcpyHostToDevice) );
	HANDLE_ERROR( cudaMemcpy(particleSystem.phaseIDs + startingIndex, &particlePhaseIDs[0], numFluidParticles * sizeof(int), cudaMemcpyHostToDevice) );
}

Fluid::Fluid(bool hose)
{
	numFluidParticles = 0;
	HANDLE_ERROR( cudaMalloc((void**)&cudaFluidParticles, MAX_FLUID_PARTICLES * sizeof(int)) );
}


__global__ void CudaHose(int width, int numParticles, ParticleSystem particleSystem, int numFluidParticles, int *cudaFluidParticles, float particleDiameter)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	int x = tid % width;
	int y = tid / width;

//	if (glm::distance(glm::vec2(x,y), glm::vec2(width/2.0f, width/2.0f)) < (float)width)
	if (tid < (width*width))
	{
		glm::vec3 centre = glm::vec3(-1.5f, 1.0f, 0.0f);
		glm::vec3 bottomLeft = glm::vec3(centre.x, centre.y - ((width/2.0f) * particleDiameter), centre.z - ((width/2.0f) * particleDiameter));

//		printf("%d\n", numParticles + tid);

		cudaFluidParticles[numFluidParticles + tid] = numParticles + tid;
		particleSystem.positions[numParticles + tid] = glm::vec3(centre.x, bottomLeft.y + (x * (particleDiameter / 5.0f) * 4.0f), bottomLeft.z + (y * (particleDiameter / 5.0f) * 4.0f));
		particleSystem.velocities[numParticles + tid] = glm::vec3(2.0f, 0.0f, 0.0f);
		particleSystem.masses[numParticles + tid] = (4.0f / 3.0f) * glm::pi<float>() * (glm::pow((particleDiameter / 2.0f), 3.0f)) * REST_DENSITY;
		particleSystem.phaseIDs[numParticles + tid] = 0;
	}
}

void Fluid::Hose(ParticleSystem &particleSystem)
{
	int width = 5;

	CudaHose<<<((width*width) + (threadsPerBlock - 1)), threadsPerBlock>>> (width, particleSystem.numParticles, particleSystem, numFluidParticles, cudaFluidParticles, particleDiameter);
	cudaDeviceSynchronize();

	particleSystem.numParticles += width*width;
	numFluidParticles += width*width;
}

__device__ float CudaCalculatePolyKernel(glm::vec3 p1, glm::vec3 p2)
{
	float dist = glm::distance(p1, p2);

	return (315.0f / (64.0f * glm::pi<float>() * PARTICLE_DIAMETER_9)) * glm::pow((PARTICLE_DIAMETER_2 - (dist*dist)), 3.0f);
}


__device__ float CudaCalculateDensity(ParticleSystem particleSystem, int index, int* neighbours, int numCurrentNeighbours)
{
	float density = 0.0f;

	for (int i=0; i<numCurrentNeighbours; i++)
	{
		density += particleSystem.masses[neighbours[(index * MAX_NEIGHBOURS) + i]] * CudaCalculatePolyKernel(particleSystem.newPositions[index], particleSystem.newPositions[neighbours[(index * MAX_NEIGHBOURS) + i]]);
	}

	density += particleSystem.masses[index] * CudaCalculatePolyKernel(particleSystem.newPositions[index], particleSystem.newPositions[index]);
	return density;
}

__device__ glm::vec3 CudaCalculateSpikyKernel(glm::vec3 p1, glm::vec3 p2)
{
	glm::vec3 r = p1 - p2;
	float dist = glm::distance(p1, p2);

	if (dist > 0.0f)
		return (45.0f / (glm::pi<float>() * PARTICLE_DIAMETER_6)) * (r / (dist + 0.0001f)) * glm::pow((PARTICLE_DIAMETER - dist), 2.0f);
	else
		return glm::vec3(0.0f);
}



__global__ void CudaCalculateLambda(int numFluidParticles, int* cudaFluidParticles, ParticleSystem particleSystem, int* neighbours, int* numNeighbours)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numFluidParticles)
	{
		int particleID = cudaFluidParticles[tid];
		glm::vec3 pos = particleSystem.newPositions[particleID];

		int numCurrentNeighbours = numNeighbours[particleID];

		float density = CudaCalculateDensity(particleSystem, particleID, neighbours, numCurrentNeighbours);
		particleSystem.densities[particleID] = density;

		float constraint = (density / REST_DENSITY) - 1.0f;
		constraint = glm::max(constraint, 0.0f);

		float gradConstraint, sumGradients = 0.0f;

		glm::vec3 accum = glm::vec3(0.0f);
		glm::vec3 pos2;
		
		for (int i=0; i<numCurrentNeighbours; i++)
		{
			pos2 = particleSystem.newPositions[neighbours[(particleID * MAX_NEIGHBOURS) + i]];

			gradConstraint = glm::length(CudaCalculateSpikyKernel(pos, pos2) * (-1.0f / REST_DENSITY));
			sumGradients += gradConstraint * gradConstraint;

			accum += CudaCalculateSpikyKernel(pos, pos2);
		}
		

		gradConstraint = glm::length(accum / REST_DENSITY);
		sumGradients += gradConstraint * gradConstraint;

		particleSystem.lambda[particleID] = -constraint / (sumGradients + RELAXATION);
	}
}

__global__ void CudaCalculateDeltaPos(int numFluidParticles, int* cudaFluidParticles, ParticleSystem particleSystem, int* neighbours, int* numNeighbours)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numFluidParticles)
	{
		int particleID = cudaFluidParticles[tid];
		int particle2ID;

		int numCurrentNeighbours = numNeighbours[particleID];

		particleSystem.deltaPositions[particleID] = glm::vec3(0.0f);

		glm::vec3 pos = particleSystem.newPositions[particleID];
		float lambda = particleSystem.lambda[particleID];

		for (int i=0; i<numCurrentNeighbours; i++)
		{
			particle2ID = neighbours[(particleID * MAX_NEIGHBOURS) + i];

			particleSystem.deltaPositions[particleID] -= (lambda + particleSystem.lambda[particle2ID]) * CudaCalculateSpikyKernel(pos, particleSystem.newPositions[particle2ID]);				//Should this be minus?
		}

		particleSystem.deltaPositions[particleID] /= REST_DENSITY;
	}
}


__device__ glm::vec3 CudaCalculateSpline(glm::vec3 p1, glm::vec3 p2)
{
	float dist = glm::distance(p1, p2);
	float constant = 32.0f / (glm::pi<float>() * PARTICLE_DIAMETER_9);
	float RHS;

	if (((2.0f * dist) > PARTICLE_DIAMETER) && (dist <= PARTICLE_DIAMETER))
		RHS = glm::pow((PARTICLE_DIAMETER - dist), 3.0f) * glm::pow(dist, 3.0f);
	
	else if ((dist > 0.0f) && ((2.0f * dist) <= PARTICLE_DIAMETER))
		RHS = (2.0f * glm::pow((PARTICLE_DIAMETER - dist), 3.0f) * glm::pow(dist, 3.0f)) - (PARTICLE_DIAMETER_6 / 64.0f);

	else
		RHS = 0.0f;

	return constant * RHS * ((p1 - p2) / dist);
}



void Fluid::Constrain(ParticleSystem &particleSystem, int* neighbours, int* numNeighbours)
{
	CudaCalculateLambda <<< (numFluidParticles + 255) / 256, 256>>> (numFluidParticles, cudaFluidParticles, particleSystem, neighbours, numNeighbours);

	CudaCalculateDeltaPos <<< (numFluidParticles + 255) / 256, 256>>> (numFluidParticles, cudaFluidParticles, particleSystem, neighbours, numNeighbours);
}



__global__ void CudaCalculateSurfaceTension(int numFluidParticles, int* cudaFluidParticles, ParticleSystem particleSystem, int* neighbours, int* numNeighbours, float timestep)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numFluidParticles)
	{
		int particleID = cudaFluidParticles[tid];
		int particle2ID;
		float tensionConstant = 1.0f;

		int numCurrentNeighbours = numNeighbours[particleID];

		glm::vec3 cohesion = glm::vec3(0.0f);
		glm::vec3 surfaceMinzation = glm::vec3(0.0f);

		for (int i=0; i<numCurrentNeighbours; i++)
		{
			particle2ID = neighbours[(particleID * MAX_NEIGHBOURS) + i];

			cohesion += particleSystem.masses[particle2ID] * CudaCalculateSpline(particleSystem.newPositions[particleID], particleSystem.newPositions[particle2ID]);		
			
			surfaceMinzation += (particleSystem.curvatureNormals[particleID] - particleSystem.curvatureNormals[particle2ID]);
		}

		cohesion *= -tensionConstant * particleSystem.masses[particleID];
		surfaceMinzation *= -tensionConstant * particleSystem.masses[particleID];


		particleSystem.velocities[particleID] += (cohesion + surfaceMinzation) * timestep;			

	}
}


__global__ void CudaCalculateCurvatureNormal(int numFluidParticles, int* cudaFluidParticles, ParticleSystem particleSystem, int* neighbours, int* numNeighbours)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numFluidParticles)
	{
		int particleID = cudaFluidParticles[tid];
		int particle2ID;

		int numCurrentNeighbours = numNeighbours[particleID];
		glm::vec3 normal = glm::vec3(0.0f);

		for (int i=0; i<numCurrentNeighbours; i++)
		{
			particle2ID = neighbours[(particleID * MAX_NEIGHBOURS) + i];

			normal += (particleSystem.masses[particle2ID] / particleSystem.densities[particle2ID]) * CudaCalculateSpikyKernel(particleSystem.newPositions[particleID], particleSystem.newPositions[particle2ID]);
		}
		particleSystem.curvatureNormals[particleID] = PARTICLE_DIAMETER * normal;
	}
}



__global__ void CudaCalculateViscosity(int numFluidParticles, int* cudaFluidParticles, ParticleSystem particleSystem, int* neighbours, int* numNeighbours, float timestep)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numFluidParticles)
	{
		int particleID = cudaFluidParticles[tid];
		int numCurrentNeighbours = numNeighbours[particleID];
		int particle2ID;
		float c = 0.01f;
		glm::vec3 viscosity = glm::vec3(0.0f);

		
		for (int i=0; i<numCurrentNeighbours; i++)
		{
			particle2ID = neighbours[(particleID * MAX_NEIGHBOURS) + i];

			viscosity += (particleSystem.velocities[particle2ID] - particleSystem.velocities[particleID]) * CudaCalculatePolyKernel(particleSystem.newPositions[particleID], particleSystem.newPositions[particle2ID]);
		}

		particleSystem.velocities[particleID] += (c * viscosity) * timestep;

	}
}


__global__ void CudaCalculateCurl(int numFluidParticles, int* cudaFluidParticles, ParticleSystem particleSystem, int* neighbours, int* numNeighbours)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numFluidParticles)
	{
		int particleID = cudaFluidParticles[tid];
		int numCurrentNeighbours = numNeighbours[particleID];
		int particle2ID;
		glm::vec3 accum = glm::vec3(0.0f);
		
		for (int i=0; i<numCurrentNeighbours; i++)
		{
			particle2ID = neighbours[(particleID * MAX_NEIGHBOURS) + i];

			accum += glm::cross((particleSystem.velocities[particle2ID] - particleSystem.velocities[particleID]), CudaCalculateSpikyKernel(particleSystem.newPositions[particleID], particleSystem.newPositions[particle2ID]));
		}

		particleSystem.curls[particleID] = accum;
	}
}


__global__ void CudaCalculateVorticity(int numFluidParticles, int* cudaFluidParticles, ParticleSystem particleSystem, int* neighbours, int* numNeighbours, float timestep)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numFluidParticles)
	{
		int particleID = cudaFluidParticles[tid];
		int numCurrentNeighbours = numNeighbours[particleID];
		int particle2ID;

		glm::vec3 gradW = glm::vec3(0.0f);
		float magW = 0.0f;
		glm::vec3 vLoc = glm::vec3(0.0f);
		
		for (int i=0; i<numCurrentNeighbours; i++)
		{
			particle2ID = neighbours[(particleID * MAX_NEIGHBOURS) + i];
	
			glm::vec3 r = particleSystem.newPositions[particle2ID] - particleSystem.newPositions[particleID];
			
			magW = glm::distance(particleSystem.curls[particleID], particleSystem.curls[particle2ID]);
	
			if (r.x != 0.0f)
				gradW.x += magW / r.x;
			if (r.y != 0.0f)
				gradW.y += magW / r.y;
			if (r.z != 0.0f)
				gradW.z += magW / r.z;

//			gradW += particleSystem.curls[particle2ID] * CudaCalculateSpikyKernel(particleSystem.newPositions[particleID], particleSystem.newPositions[particle2ID]);
	
		}



		glm::vec3 N = gradW / (glm::length(gradW) + 0.0001f);
		glm::vec3 vorticity = RELAXATION * glm::cross(N, particleSystem.curls[particleID]);

		particleSystem.velocities[particleID] += vorticity * timestep;
	}
}


void Fluid::ModifyVelocity(ParticleSystem &particleSystem, int* neighbours, int* numNeighbours, float timestep)
{
	CudaCalculateCurvatureNormal <<< (numFluidParticles + 255) / 256, 256>>> (numFluidParticles, cudaFluidParticles, particleSystem, neighbours, numNeighbours);
	CudaCalculateSurfaceTension <<< (numFluidParticles + 255) / 256, 256>>> (numFluidParticles, cudaFluidParticles, particleSystem, neighbours, numNeighbours, timestep);
//
//	CudaCalculateCurl <<< (numFluidParticles + 255) / 256, 256>>> (numFluidParticles, cudaFluidParticles, particleSystem, neighbours, numNeighbours);
//	CudaCalculateVorticity <<< (numFluidParticles + 255) / 256, 256>>> (numFluidParticles, cudaFluidParticles, particleSystem, neighbours, numNeighbours, timestep);

	CudaCalculateViscosity <<< (numFluidParticles + 255) / 256, 256>>> (numFluidParticles, cudaFluidParticles, particleSystem, neighbours, numNeighbours, timestep);
}


