#include "FluidRenderer.h"

void FluidRenderer::Init(int xRes, int yRes)
{
	screenWidth = xRes;
	screenHeight = yRes;

	layer = finalLayer;

	frameBuffer = NewFrameBuffer();
	GenerateTextures();
}

void FluidRenderer::GenerateTextures()
{
	sphereTex = GenerateBufferTexture();
	depthTex = GenerateBufferTexture();
	XdepthBlurTex = GenerateBufferTexture();
	depthBlurTex = GenerateBufferTexture();
	normalsBlurTex = GenerateBufferTexture();
	thickTex = GenerateBufferTexture();
	thickBlurTex = GenerateBufferTexture();
	XthickBlurTex = GenerateBufferTexture();
}

GLuint FluidRenderer::NewFrameBuffer()
{
	GLuint fb;
	glGenFramebuffers (1, &fb);
	glBindFramebuffer (GL_FRAMEBUFFER, fb);

	unsigned int rb = 0;
	glGenRenderbuffers (1, &rb);
	glBindRenderbuffer (GL_RENDERBUFFER, rb);
	glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT, screenWidth, screenHeight);
	glFramebufferRenderbuffer (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb);

	glBindFramebuffer (GL_FRAMEBUFFER, 0);

	return fb;
}

void FluidRenderer::ChangeTexture(GLuint tex)
{
	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
}



GLuint FluidRenderer::GenerateBufferTexture()
{
	GLuint tex;

	glGenTextures(1, &tex);
	glBindTexture (GL_TEXTURE_2D, tex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA32F, screenWidth, screenHeight, 0, GL_RGBA, GL_FLOAT, NULL);

	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

	return tex;
}


void FluidRenderer::RenderFrameBuffer(GLuint fromTex, GLuint toTex, Camera &camera, GLuint shaderProgramID)
{
	ChangeTexture(toTex);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	int uniformLoc;
	glm::mat4 model_mat = glm::mat4(1.0f);
	uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.projMat[0][0]);
	uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.viewMat[0][0]);
	uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &model_mat[0][0]);

	glActiveTexture (GL_TEXTURE0);
	int texLoc = glGetUniformLocation(shaderProgramID,"texColor");
	glUniform1i(texLoc,0);
	glBindTexture (GL_TEXTURE_2D, fromTex);	
	int texcoords_location = glGetAttribLocation (shaderProgramID, "vTexture");

	glBegin(GL_QUADS);
	glVertexAttrib2f(texcoords_location, 0.0f, 0.0f);
	glVertex2f(-1.0f, -1.0f);

	glVertexAttrib2f(texcoords_location, 1.0f, 0.0f);
	glVertex2f(1.0f, -1.0f);

	glVertexAttrib2f(texcoords_location, 1.0f, 1.0f);
	glVertex2f(1.0f, 1.0f);

	glVertexAttrib2f(texcoords_location, 0.0f, 1.0f);
	glVertex2f(-1.0f, 1.0f);
	glEnd();
}


void FluidRenderer::Render(PhysicsWorld &physicsWorld, EnvironmentMap &environmentMap, Camera &camera, ShaderManager &shaderManager)
{
	GLuint shaderProgramID;

	//Render Spheres
	shaderProgramID = shaderManager.UseShader(SphereShader);
	glBindFramebuffer (GL_FRAMEBUFFER, frameBuffer);
	ChangeTexture(sphereTex);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	DrawSpheres(physicsWorld, camera, shaderProgramID);

	//Find depth
	shaderProgramID = shaderManager.UseShader(DepthShader);
	ChangeTexture(depthTex);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	DrawSpheres(physicsWorld, camera, shaderProgramID);
	
	
	//Blur Depth in X
	shaderProgramID = shaderManager.UseShader(BlurShader);

	int layer_loc = glGetUniformLocation(shaderProgramID, "displayLayer");
	glUniform1i(layer_loc, (int)layer);

	int blurDir_loc = glGetUniformLocation(shaderProgramID, "blurDirection");
	glUniform2f(blurDir_loc, 1.0f, 0.0f);
	RenderFrameBuffer(depthTex, XdepthBlurTex, camera, shaderProgramID);
	//Second pass
	glUniform2f(blurDir_loc, 0.0f, 1.0f);
	RenderFrameBuffer(XdepthBlurTex, depthBlurTex, camera, shaderProgramID);

	//Get Normals
	shaderProgramID = shaderManager.UseShader(NormalsShader);
	RenderFrameBuffer(depthBlurTex, normalsBlurTex, camera, shaderProgramID);
	
	//Find thickness
	ChangeTexture(thickTex);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shaderProgramID = shaderManager.UseShader(ThicknessShader);
	glDisable (GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	DrawSpheres(physicsWorld, camera, shaderProgramID);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	//Blur thickness
	shaderProgramID = shaderManager.UseShader(BlurShader);
	blurDir_loc = glGetUniformLocation(shaderProgramID, "blurDirection");
	glUniform2f(blurDir_loc, 1.0f, 0.0f);
	RenderFrameBuffer(thickTex, XthickBlurTex, camera, shaderProgramID);
	//Second pass
	glUniform2f(blurDir_loc, 0.0f, 1.0f);
	RenderFrameBuffer(XthickBlurTex, thickBlurTex, camera, shaderProgramID);

	//Final render
	shaderProgramID = shaderManager.UseShader(FinalRenderShader);
	RenderFinalBuffer(environmentMap, camera, shaderProgramID);
	
	
}


void FluidRenderer::RenderFinalBuffer(EnvironmentMap &environmentMap, Camera &camera, GLuint shaderProgramID)
{
	glBindFramebuffer (GL_FRAMEBUFFER, 0);
//	glClearColor(0.2, 0.2, 0.2, 1.0);
//	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	int uniformLoc;
	glm::mat4 model_mat = glm::mat4(1.0f);
	uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.projMat[0][0]);
	uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.viewMat[0][0]);
	uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &model_mat[0][0]);

	int layer_loc = glGetUniformLocation(shaderProgramID, "displayLayer");
	glUniform1i(layer_loc, (int)layer);

	int texcoords_location = glGetAttribLocation (shaderProgramID, "vTexture");

	glActiveTexture (GL_TEXTURE0);
	int texLoc = glGetUniformLocation(shaderProgramID,"depthTex");
	glUniform1i(texLoc,0);
	glBindTexture (GL_TEXTURE_2D, depthTex);

	glActiveTexture (GL_TEXTURE1);
	texLoc = glGetUniformLocation(shaderProgramID,"depthBlurTex");
	glUniform1i(texLoc,1);
	glBindTexture (GL_TEXTURE_2D, depthBlurTex);

	glActiveTexture (GL_TEXTURE2);
	texLoc = glGetUniformLocation(shaderProgramID,"normalsBlurTex");
	glUniform1i(texLoc,2);
	glBindTexture (GL_TEXTURE_2D, normalsBlurTex);

	glActiveTexture (GL_TEXTURE3);
	texLoc = glGetUniformLocation(shaderProgramID,"thickTex");
	glUniform1i(texLoc,3);
	glBindTexture (GL_TEXTURE_2D, thickTex);

	glActiveTexture (GL_TEXTURE4);
	texLoc = glGetUniformLocation(shaderProgramID,"thickBlurTex");
	glUniform1i(texLoc,4);
	glBindTexture (GL_TEXTURE_2D, thickBlurTex);

	glActiveTexture(GL_TEXTURE5);
	texLoc = glGetUniformLocation(shaderProgramID,"envMap");
	glUniform1i(texLoc,5);
	glBindTexture(GL_TEXTURE_CUBE_MAP, environmentMap.m_textureObj);

	glActiveTexture(GL_TEXTURE6);
	texLoc = glGetUniformLocation(shaderProgramID,"sphereTex");
	glUniform1i(texLoc,6);
    glBindTexture(GL_TEXTURE_2D, sphereTex);

	glActiveTexture (GL_TEXTURE7);
	texLoc = glGetUniformLocation(shaderProgramID,"vertDepth");
	glUniform1i(texLoc,7);
	glBindTexture (GL_TEXTURE_2D, depthTex);
	

	glBegin(GL_QUADS);
	glVertexAttrib2f(texcoords_location, 0.0f, 0.0f);
	glVertex2f(-1.0f, -1.0f);

	glVertexAttrib2f(texcoords_location, 1.0f, 0.0f);
	glVertex2f(1.0f, -1.0f);

	glVertexAttrib2f(texcoords_location, 1.0f, 1.0f);
	glVertex2f(1.0f, 1.0f);

	glVertexAttrib2f(texcoords_location, 0.0f, 1.0f);
	glVertex2f(-1.0f, 1.0f);
	glEnd();
}


void FluidRenderer::DrawSpheres(PhysicsWorld &physicsWorld, Camera &camera, GLuint shaderProgramID)
{
	int uniformLoc;

	glm::mat4 model_mat = glm::mat4(1.0f);

	uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.projMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.viewMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &model_mat[0][0]);

	int texcoords_location = glGetAttribLocation (shaderProgramID, "vTexture");
	int radius_location = glGetAttribLocation (shaderProgramID, "pRadius");	

//	glm::mat4 upMat = glm::rotate(glm::mat4(), -camera.pitch, glm::vec3(1, 0, 0));
//	glm::vec3 upVec = glm::rotate(glm::quat_cast(upMat), glm::vec3(0, 1, 0));

	glm::vec3 camDirection = camera.GetViewDirection();

	glm::vec3 rightVec = glm::normalize(glm::cross(camDirection, glm::vec3(0.0f, 1.0f, 0.0f)));
	glm::vec3 upVec = glm::normalize(glm::cross(camDirection, -rightVec));
	
	glBegin(GL_QUADS);
	for (int i=0; i<physicsWorld.fluid.numFluidParticles; i++)
	{
		glm::vec3 posToCam = glm::normalize(physicsWorld.hostPositions[i] - camera.position);
		glm::vec3 crossCam = glm::cross(posToCam, upVec);

		float radius = particleRadius * 1.0f;
		glVertexAttrib1f(radius_location, radius);

		glm::vec3 corner = physicsWorld.hostPositions[i] - (radius * crossCam);
		corner += radius*upVec;

		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertexAttrib2f(texcoords_location, 0.0f, 0.0f);
		glVertex3f(corner.x, corner.y, corner.z);

		corner -= 2.0f*radius*upVec;

		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertexAttrib2f(texcoords_location, 0.0f, 1.0f);
		glVertex3f(corner.x, corner.y, corner.z);

		corner = physicsWorld.hostPositions[i] + (radius * crossCam);
		corner -= radius*upVec;

		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertexAttrib2f(texcoords_location, 1.0f, 1.0f);
		glVertex3f(corner.x, corner.y, corner.z);

		corner += 2.0f*radius*upVec;

		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertexAttrib2f(texcoords_location, 1.0f, 0.0f);
		glVertex3f(corner.x, corner.y, corner.z);
	}
	glEnd();

}