#ifndef CLOTH_H
#define CLOTH_H

#include "CudaHeaders.h"
#include "GLM.h"
#include <vector>
#include "ParticleSystem.h"
#include <GL/glew.h>

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

class Cloth
{
public:
	int numClothParticles;
	int particlesPerSide;
	float particleDiameter;
	float diagonalDistance;
	float stretchStiffness;
	int startingIndex;

	std::vector<int> clothParticleIndices;

	int* cudaClothParticles;
	int *cudaMatchedEdges;
	int *cudaMatchedDiagonals;
	glm::vec3 *cudaVertNormals;
	std::vector<glm::vec3> emptyNormals;

	GLuint vboID, vaoID;
	int numTriangleVerts;
	std::vector<int> triangleIndices;
	int *cudaTriangleIndices;	

	Cloth();
	Cloth(glm::vec3 position, int particlesPerSide_, float particleDiameter_, ParticleSystem &particleSystem, float stretchStiffness_);

	void InitBufferObjects();
	void CalculateTriangles();

	void ClampPoints(ParticleSystem &particleSystem);
	void Constrain(ParticleSystem &particleSystem);
	void MatchParticles(int *edgesArray, int *diagonalsArray);
	void Aerodynamics(ParticleSystem &particleSystem, float timestep);

	void Draw(GLuint shaderProgramID, ParticleSystem &particleSystem);
	void CalculateNormals(ParticleSystem &particleSystem);
};

#endif