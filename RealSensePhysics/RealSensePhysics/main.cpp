#include <GL/glew.h>
#include <GL/freeglut.h>
#include <iostream>
#include "Scene.h"

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

Scene scene;

// For mouse control
int xclickedAt = -1;
int yclickedAt = -1;




void Init()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_DEPTH_TEST);
	
	scene.Init(SCREEN_WIDTH, SCREEN_HEIGHT);
}



// Update renderer and check for Qt events
void Update()
{
	scene.Update();
}




// General Keyboard Input
void KeyboardFunc (unsigned char key, int xmouse, int ymouse)
{
	switch(key)
	{
		case '1':
			scene.fluidRenderer.layer = depthLayer;
			break;
		case '2':
			scene.fluidRenderer.layer = depthBlurLayer;
			break;
		case '3':
			scene.fluidRenderer.layer = normalsLayer;
			break;
		case '4':
			scene.fluidRenderer.layer = thickLayer;
			break;
		case '5':
			scene.fluidRenderer.layer = thickBlurLayer;
			break;
		case '6':
			scene.fluidRenderer.layer = reflectLayer;
			break;
		case '7':
			scene.fluidRenderer.layer = refractLayer;
			break;
		case '8':
			scene.fluidRenderer.layer = finalLayer;
			break;
		case '9':
			scene.fluidRenderer.layer = colourLayer;
			break;
		case '0':
			scene.fluidRenderer.layer = sphereLayer;
			break;
		case 'h':
			if (!scene.physicsWorld.hoseOn)
			{
				scene.physicsWorld.lastHoseTime = clock();
				scene.physicsWorld.hoseOn = true;
			}
			else
				scene.physicsWorld.hoseOn = false;
			break;
		case 27:
			cudaDeviceReset();
			exit(0);
			break;
	}
}


// Keyboad Special Keys Input
void SpecialFunc(int key, int x, int y)
 {
	switch(key)
	{
	case GLUT_KEY_UP:
		scene.camera.position.z -= 0.5f;
		break;
	case GLUT_KEY_LEFT:
		scene.camera.position.x -= 0.5f;
		break;
	case GLUT_KEY_DOWN:
		scene.camera.position.z += 0.5f;
		break;
	case GLUT_KEY_RIGHT:
		scene.camera.position.x += 0.5f;
		break;
	case GLUT_KEY_PAGE_UP:
		scene.camera.position.y += 0.5f;
		break;
	case GLUT_KEY_PAGE_DOWN:
		scene.camera.position.y -= 0.5f;
		break;
	}
	glutPostRedisplay();
 }


// Mouse drags to control camera
void MouseMove(int x, int y) 
{ 	
	if (xclickedAt >= 0)
	{
		scene.camera.Rotate((float)(xclickedAt - x));
		xclickedAt = x;
	}
		

	if (yclickedAt >= 0)
	{
		scene.camera.Translate(glm::vec3(0.0f, (y - yclickedAt) * 0.1f, 0.0f));
		yclickedAt = y;
	}
}


// Mouse clicks to initialize drags
void MouseButton(int button, int state, int x, int y) 
{
	if (button == GLUT_RIGHT_BUTTON) 
	{
		if (state == GLUT_UP)
		{
			xclickedAt = -1;
			yclickedAt = -1;
		}
		else
		{
			xclickedAt = x;
			yclickedAt = y;
		}
	}
}


// Mouse wheel to zoom camera
void MouseWheel(int wheel, int direction, int x, int y) 
{
	scene.camera.Zoom(-direction * 0.2f);	
}




int main(int argc, char *argv[])
{
	// Set up the window
	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    glutCreateWindow("Fluid Simulation");

	// Tell glut where the display function is
	glutDisplayFunc(Update);
	glutIdleFunc(Update);

	// A call to glewInit() must be done after glut is initialized!
    GLenum res = glewInit();
	// Check for any errors
    if (res != GLEW_OK) 
	{
		fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
		return 1;
    }
		
	// Initialize Renderer and Qt
	Init();


	// Specify glut input functions
	glutKeyboardFunc(KeyboardFunc);
	glutSpecialFunc(SpecialFunc);
	glutMouseFunc(MouseButton);
	glutMotionFunc(MouseMove);
	glutMouseWheelFunc(MouseWheel);

	// Begin infinite event loop
	glutMainLoop();
	
    return 0;

}