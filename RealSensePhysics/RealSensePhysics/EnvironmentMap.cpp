#include "EnvironmentMap.h"

void EnvironmentMap::Init()
{
	InitBufferObjects();
//	frameBuffer = NewFrameBuffer();
//
//	fbTex1 = GenerateBufferTexture();
//	fbTex2 = GenerateBufferTexture();

	Load();
}

void EnvironmentMap::InitBufferObjects()
{
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);

	// Set up array buffers
	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);

	cubePoints.reserve(36);

	cubePoints.push_back(glm::vec3(-10.0f,  10.0f, -10.0f));
	cubePoints.push_back(glm::vec3(-10.0f, -10.0f, -10.0f));
	cubePoints.push_back(glm::vec3( 10.0f, -10.0f, -10.0f));
	cubePoints.push_back(glm::vec3( 10.0f, -10.0f, -10.0f));
	cubePoints.push_back(glm::vec3( 10.0f,  10.0f, -10.0f));
	cubePoints.push_back(glm::vec3(-10.0f,  10.0f, -10.0f));

	cubePoints.push_back(glm::vec3(-10.0f, -10.0f,  10.0f));
	cubePoints.push_back(glm::vec3(-10.0f, -10.0f, -10.0f));
	cubePoints.push_back(glm::vec3(-10.0f,  10.0f, -10.0f));
	cubePoints.push_back(glm::vec3(-10.0f,  10.0f, -10.0f));
	cubePoints.push_back(glm::vec3(-10.0f,  10.0f,  10.0f));
	cubePoints.push_back(glm::vec3(-10.0f, -10.0f,  10.0f));

	cubePoints.push_back(glm::vec3( 10.0f, -10.0f, -10.0f));
	cubePoints.push_back(glm::vec3( 10.0f, -10.0f,  10.0f));
	cubePoints.push_back(glm::vec3( 10.0f,  10.0f,  10.0f));
	cubePoints.push_back(glm::vec3( 10.0f,  10.0f,  10.0f));
	cubePoints.push_back(glm::vec3( 10.0f,  10.0f, -10.0f));
	cubePoints.push_back(glm::vec3( 10.0f, -10.0f, -10.0f));

	cubePoints.push_back(glm::vec3(-10.0f, -10.0f,  10.0f));
	cubePoints.push_back(glm::vec3(-10.0f,  10.0f,  10.0f));
	cubePoints.push_back(glm::vec3( 10.0f,  10.0f,  10.0f));
	cubePoints.push_back(glm::vec3( 10.0f,  10.0f,  10.0f));
	cubePoints.push_back(glm::vec3( 10.0f, -10.0f,  10.0f));
	cubePoints.push_back(glm::vec3(-10.0f, -10.0f,  10.0f));

	cubePoints.push_back(glm::vec3(-10.0f,  10.0f, -10.0f));
	cubePoints.push_back(glm::vec3( 10.0f,  10.0f, -10.0f));
	cubePoints.push_back(glm::vec3( 10.0f,  10.0f,  10.0f));
	cubePoints.push_back(glm::vec3( 10.0f,  10.0f,  10.0f));
	cubePoints.push_back(glm::vec3(-10.0f,  10.0f,  10.0f));
	cubePoints.push_back(glm::vec3(-10.0f,  10.0f, -10.0f));

	cubePoints.push_back(glm::vec3(-10.0f, -10.0f, -10.0f));
	cubePoints.push_back(glm::vec3(-10.0f, -10.0f,  10.0f));
	cubePoints.push_back(glm::vec3( 10.0f, -10.0f, -10.0f));
	cubePoints.push_back(glm::vec3( 10.0f, -10.0f, -10.0f));
	cubePoints.push_back(glm::vec3(-10.0f, -10.0f,  10.0f));
	cubePoints.push_back(glm::vec3( 10.0f, -10.0f,  10.0f));

	glBufferData(GL_ARRAY_BUFFER, 36 * sizeof(glm::vec3), &cubePoints[0], GL_STATIC_DRAW);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

}



void EnvironmentMap::Load()
{
	
	std::vector<std::string> cubeFaces;
	
	cubeFaces.push_back("xpos.png");
	cubeFaces.push_back("xneg.png");
	cubeFaces.push_back("ypos.png");
	cubeFaces.push_back("yneg.png");
	cubeFaces.push_back("zneg.png");
	cubeFaces.push_back("zpos.png");
	


	
	cube.push_back(GL_TEXTURE_CUBE_MAP_POSITIVE_X);
	cube.push_back(GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
	cube.push_back(GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
	cube.push_back(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
	cube.push_back(GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
	cube.push_back(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);

	glGenTextures(1, &m_textureObj);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureObj);

	for (unsigned int i=0; i<6; i++)
	{	
//		glTexImage2D(cube[i], 0, GL_RGBA, 800, 800, 0 ,GL_BGRA, GL_UNSIGNED_BYTE, NULL);
		
		ilInit();
		ilGenImages(1,&texid);
		ilBindImage(texid);

		success = ilLoadImage((const ILstring)cubeFaces[i].c_str());

		if(success)
		{
			success = ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE);
			glTexImage2D(cube[i], 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData());
		}

		ilDeleteImages(1,&texid);
		
	}


	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


	glEnable(GL_TEXTURE_CUBE_MAP);

}


void EnvironmentMap::Draw(Camera &camera, ShaderManager &shaderManager)
{
	GLuint shaderProgramID = shaderManager.UseShader(SkyShader);

	int uniformLoc;

	glm::mat4 model_mat = glm::mat4(1.0f);

	uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.projMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.viewMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &model_mat[0][0]);

	glBindVertexArray(vaoID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);

	GLuint vPosition = glGetAttribLocation(shaderProgramID, "blah");
	glEnableVertexAttribArray(vPosition);	
	glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glActiveTexture(GL_TEXTURE0);
	int texLoc = glGetUniformLocation(shaderProgramID,"Cubemap");
	glUniform1i(texLoc,0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureObj);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}




void EnvironmentMap::Update(Camera &camera)
{
//	glm::vec3 centrePoint = glm::vec3(0.0f, 0.5f, -1.0f);
//
//	GLuint shaderProgramID = camera.useShader(ModelShader);
//	int sky_proj_mat_location = glGetUniformLocation (shaderProgramID, "proj");
//	glUniformMatrix4fv (sky_proj_mat_location, 1, GL_FALSE, &camera.sky_proj_mat[0][0]);
//	int sky_view_mat_location = glGetUniformLocation (shaderProgramID, "view");
//	glm::mat4 sky_view_mat;
//	glm::mat4 T, Y, P;
//	T = glm::translate (glm::mat4(), -centrePoint);
//
//	glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureObj);
//
//	glBindFramebuffer (GL_FRAMEBUFFER, frameBuffer);
//	
//	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTex1, 0);
//	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	
//	Y = glm::rotate (glm::mat4(), 0.0f, glm::vec3(0,1,0));
//	P = glm::rotate (glm::mat4(), 0.0f, glm::vec3(1,0,0));
//	sky_view_mat = Y * P * T;
//	glUniformMatrix4fv (sky_view_mat_location, 1, GL_FALSE, &sky_view_mat[0][0]);
//
//	kinect.DrawMesh(shaderProgramID);
//	FlipTexture(camera, fbTex1, fbTex2);
//	glCopyTexSubImage2D(cube[5], 0, 0, 0, 0, 0, 800, 800);
//
//
//	shaderProgramID = camera.useShader(ModelShader);
//	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTex1, 0);
//	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	Y = glm::rotate (glm::mat4(), 90.0f, glm::vec3(0,1,0));
//	P = glm::rotate (glm::mat4(), 0.0f, glm::vec3(1,0,0));
//	sky_view_mat = Y * P * T;
//	glUniformMatrix4fv (sky_view_mat_location, 1, GL_FALSE, &sky_view_mat[0][0]);
//
//	kinect.DrawMesh(shaderProgramID);
//	FlipTexture(camera, fbTex1, fbTex2);
//	glCopyTexSubImage2D(cube[0], 0, 0, 0, 0, 0, 800, 800);
//	
//
//	shaderProgramID = camera.useShader(ModelShader);
//	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTex1, 0);
//	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	Y = glm::rotate (glm::mat4(), -90.0f, glm::vec3(0,1,0));
//	P = glm::rotate (glm::mat4(), 0.0f, glm::vec3(1,0,0));
//	sky_view_mat = Y * P * T;
//	glUniformMatrix4fv (sky_view_mat_location, 1, GL_FALSE, &sky_view_mat[0][0]);
//
//	kinect.DrawMesh(shaderProgramID);
//	FlipTexture(camera, fbTex1, fbTex2);
//	glCopyTexSubImage2D(cube[1], 0, 0, 0, 0, 0, 800, 800);
//
//
//
//	shaderProgramID = camera.useShader(ModelShader);
//	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTex1, 0);
//	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	Y = glm::rotate (glm::mat4(), 0.0f, glm::vec3(0,1,0));
//	P = glm::rotate (glm::mat4(), -90.0f, glm::vec3(1,0,0));
//	sky_view_mat = Y * P * T;
//	glUniformMatrix4fv (sky_view_mat_location, 1, GL_FALSE, &sky_view_mat[0][0]);
//
//	kinect.DrawMesh(shaderProgramID);
//	glCopyTexSubImage2D(cube[2], 0, 0, 0, 0, 0, 800, 800);
//	
//	
//	shaderProgramID = camera.useShader(ModelShader);
//	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTex1, 0);
//	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	Y = glm::rotate (glm::mat4(), 0.0f, glm::vec3(0,1,0));
//	P = glm::rotate (glm::mat4(), 90.0f, glm::vec3(1,0,0));
//	sky_view_mat = Y * P * T;
//	glUniformMatrix4fv (sky_view_mat_location, 1, GL_FALSE, &sky_view_mat[0][0]);
//
//	kinect.DrawMesh(shaderProgramID);
//	glCopyTexSubImage2D(cube[3], 0, 0, 0, 0, 0, 800, 800);
//	
//	
//	shaderProgramID = camera.useShader(ModelShader);
//	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTex1, 0);
//	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	Y = glm::rotate (glm::mat4(), 0.0f, glm::vec3(0,1,0));
//	P = glm::rotate (glm::mat4(), 180.0f, glm::vec3(1,0,0));
//	sky_view_mat = Y * P * T;
//	glUniformMatrix4fv (sky_view_mat_location, 1, GL_FALSE, &sky_view_mat[0][0]);
//
//	kinect.DrawMesh(shaderProgramID);
//	FlipTexture(camera, fbTex1, fbTex2);
//	glCopyTexSubImage2D(cube[4], 0, 0, 0, 0, 0, 800, 800);
//
//
//
//
//	glBindFramebuffer (GL_FRAMEBUFFER, 0);
//	
//	
	

}



void EnvironmentMap::FlipTexture(Camera &camera, GLuint fromTex, GLuint toTex)
{
//	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, toTex, 0);
//	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	GLuint shaderProgramID = camera.useShader(TextureShader);
//	glActiveTexture (GL_TEXTURE0);
//	int texLoc = glGetUniformLocation(shaderProgramID,"texColor");
//	glUniform1i(texLoc,0);
//	glBindTexture (GL_TEXTURE_2D, fromTex);	
//	int texcoords_location = glGetAttribLocation (shaderProgramID, "vTexture");
//
//	glBegin(GL_QUADS);
//	glVertexAttrib2f(texcoords_location, 1.0f, 1.0f);
//	glVertex2f(-1.0f, -1.0f);
//
//	glVertexAttrib2f(texcoords_location, 0.0f, 1.0f);
//	glVertex2f(1.0f, -1.0f);
//
//	glVertexAttrib2f(texcoords_location, 0.0f, 0.0f);
//	glVertex2f(1.0f, 1.0f);
//
//	glVertexAttrib2f(texcoords_location, 1.0f, 0.0f);
//	glVertex2f(-1.0f, 1.0f);
//	glEnd();
//
//	glBindTexture(GL_TEXTURE_2D, 0);
}


GLuint EnvironmentMap::NewFrameBuffer()
{
	GLuint fb;
	glGenFramebuffers (1, &fb);
	glBindFramebuffer (GL_FRAMEBUFFER, fb);

	unsigned int rb = 0;
	glGenRenderbuffers (1, &rb);
	glBindRenderbuffer (GL_RENDERBUFFER, rb);
	glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 800, 800);
	glFramebufferRenderbuffer (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb);

	glBindFramebuffer (GL_FRAMEBUFFER, 0);

	return fb;
}



GLuint EnvironmentMap::GenerateBufferTexture()
{
	GLuint tex;

	glGenTextures(1, &tex);
	glBindTexture (GL_TEXTURE_2D, tex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, 800, 800, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

	return tex;
}